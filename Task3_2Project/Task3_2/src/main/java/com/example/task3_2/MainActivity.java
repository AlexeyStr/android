package com.example.task3_2;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

    private AsyncTaskFactorial task;
    private EditText editFact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        editFact = (EditText) findViewById(R.id.editFact);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private class AsyncTaskFactorial extends AsyncTask<Integer, Void, Integer> {

        @Override
        protected Integer doInBackground(Integer... num) {
            Integer fact = 1;
            for (int i = 1; i <= num[0]; i++) {
                fact *= i;
            }
            return fact;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);

            editFact.setText(result.toString());
        }
    }

    public void onClickFact (View view){
        if (task == null || task.getStatus().equals(AsyncTask.Status.FINISHED)) {
            task = new AsyncTaskFactorial();
            try{
                task.execute(Integer.parseInt(editFact.getText().toString()));
            }catch(Exception e){
                Toast.makeText(MainActivity.this, "Введено не число", Toast.LENGTH_SHORT)
                        .show();
            }
        } else {
            Toast.makeText(MainActivity.this, "Поток не завершен", Toast.LENGTH_SHORT)
                    .show();
        }
    }

}