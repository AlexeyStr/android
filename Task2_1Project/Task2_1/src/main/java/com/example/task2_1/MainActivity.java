package com.example.task2_1;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {

    Button btnContacts;
    Button btnSMS;
    Button btnMail;
    Button btnInternet;


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        btnContacts = (Button) findViewById(R.id.Contacts);
        btnSMS = (Button) findViewById(R.id.SMS);
        btnMail = (Button) findViewById(R.id.Mail);
        btnInternet = (Button) findViewById(R.id.Internet);

        btnContacts.setOnClickListener(this);
        btnSMS.setOnClickListener(this);
        btnMail.setOnClickListener(this);
        btnInternet.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.Contacts:
                intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:12345"));
                startActivity(intent);
                break;
            case R.id.SMS:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.fromParts("sms", "12345", null));
                startActivity(intent);
                break;
            case R.id.Mail:
                intent = new Intent(Intent.ACTION_VIEW);
                //intent.setType("plain/text");
                intent.setData(Uri.fromParts("mailto", "ss@ss.ss", null));
                startActivity(intent);
                break;
            case R.id.Internet:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http://9gag.com"));
                startActivity(intent);
                break;
        }
    }
}
