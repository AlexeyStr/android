package com.example.task3_1;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

    EditText editFact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        editFact   = (EditText)findViewById(R.id.editFact);


    }

    public void onClickFact (View view){

        if (!editFact.getText().toString().matches("")){
            new Thread(new Runnable() {
                @Override
                public void run() {

                        Integer num = Integer.parseInt(editFact.getText().toString());
                        Integer fact = 1;
                        for (int i = 1; i<=num; i++){
                            fact *= i;
                        }


                        final Integer factResult = fact;


                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                editFact.setText(factResult.toString());
                            }
                        });
                    }
            }).start();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}