package com.example.task2_2;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends Activity implements OnClickListener {

    private static final int CONTACT_CODE = 1;
    private static final int IMAGE_CODE = 2;
    private static final int VIDEO_CODE = 3;
    TextView textPhone;
    private Bitmap bitmap;
    private VideoView vid;
    private ImageView imageView;
    Button btnContact;
    Button btnImg;
    Button btnVideo;

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK){

            switch(requestCode){

                case CONTACT_CODE:

                    Cursor cursor = getContentResolver().query(data.getData(),
                            new String[] {ContactsContract.Contacts.DISPLAY_NAME}, null, null, null);

                    if (cursor.moveToFirst()) {

                        int columnIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
                        String name = cursor.getString(columnIndex);
                        textPhone=(TextView)findViewById(R.id.textPhone);
                        textPhone.setText(name);

                    }
                    break;

                case IMAGE_CODE:

                    try {

                        if (bitmap != null) {
                            bitmap.recycle();
                        }

                        InputStream stream = getContentResolver().openInputStream(data.getData());
                        bitmap = BitmapFactory.decodeStream(stream);
                        stream.close();
                        imageView.setImageBitmap(bitmap);

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;

                case VIDEO_CODE:

                    vid.setVideoPath(data.getData().toString());
                    vid.start();
                    break;

            }
        }

            super.onActivityResult(requestCode, resultCode, data);

    }


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        //textPhone = (EditText)findViewById(R.id.textPhone);
        imageView = (ImageView)findViewById(R.id.imgGet);
        vid = (VideoView) findViewById(R.id.videoView);

        btnContact = (Button) findViewById(R.id.Contact);
        btnImg = (Button) findViewById(R.id.Img);
        btnVideo = (Button) findViewById(R.id.Video);

        btnContact.setOnClickListener(this);
        btnImg.setOnClickListener(this);
        btnVideo.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        //Intent intent;
        switch (v.getId()) {
            case R.id.Contact:
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setData(Uri.parse("content://contacts/people/"));
                startActivityForResult(intent, CONTACT_CODE);
                break;
            case R.id.Img:
                intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(intent, IMAGE_CODE);
                break;
            case R.id.Video:
                intent = new Intent(Intent.ACTION_PICK, null);
                intent.setType("video/*");
                startActivityForResult(intent, VIDEO_CODE);
                break;
        }
    }

}

