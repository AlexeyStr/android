package com.example.task1;

/**
 * Created by dev4 on 1/28/14.
 */
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ScreenFirst extends Activity {
    // Initializing variables
    TextView name;
    @Override
    public void onCreate(Bundle savedInstanceState) {

        //Bundle receiveBundle = this.getIntent().getExtras();
        //String receiveValue = receiveBundle.getString("name");
        /*Bundle receiveBundle = this.getIntent().getExtras();

        if(receiveBundle != null) {

            TextView receiveValueEdit = (TextView) findViewById(R.id.txtName);
            String receiveValue = receiveBundle.getString("value");

            receiveValueEdit.setText(receiveValue);

        }*/

        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen1);

        TextView txtName = (TextView) findViewById(R.id.txtName);

        Bundle receiveBundle = this.getIntent().getExtras();

        if(receiveBundle != null){

            final String receiveValue = receiveBundle.getString("value");

            txtName.setText(receiveValue);

        }

        Button btnNextScreen = (Button) findViewById(R.id.btnNextScreen);

        //txtName.setText(txtName);

        //Listening to button event
        btnNextScreen.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {

                //Starting a new Intent
                Intent nextScreen = new Intent(getApplicationContext(), ScreenSecond.class);

                startActivity(nextScreen);

            }
        });

    }
}