package com.example.task1;

/**
 * Created by dev4 on 1/28/14.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;

public class Screen1Activity extends Activity {

    Button button;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        addListenerOnButton();
    }

    public void addListenerOnButton() {

        final Context context = this;

        button = (Button) findViewById(R.id.button1);

        button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(new Intent(Screen1Activity.this, Screen2Activity.class));
                startActivityForResult(i, 0);
                Intent intent = new Intent(context, Screen2Activity.class);
                startActivity(intent);
            }

        });

    }

}
