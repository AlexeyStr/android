package com.example.task1;

/**
 * Created by dev4 on 1/28/14.
 */
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ScreenSecond extends Activity {
    /** Called when the activity is first created. */

    EditText inputName;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen2);



        Button btnClose = (Button) findViewById(R.id.btnClose);

        Intent i = getIntent();
        // Receiving the Data
        String name = i.getStringExtra("name");
        //Log.e("Second Screen", name + "." + email);

        // Displaying Received data


        btnClose.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle sendBundle = new Bundle();

                inputName = (EditText) findViewById(R.id.name);

                sendBundle.putString("value", inputName.getText().toString());

                Intent i = new Intent(ScreenSecond.this, ScreenFirst.class);
                i.putExtras(sendBundle);
                startActivity(i);
           /*
                Intent nextScreen = new Intent(ScreenSecond.this, ScreenFirst.class);

                //Sending data to another Activity
                nextScreen.putExtra("name", inputName.getText().toString());

                Log.e("n", inputName.getText()+".");

                startActivity(nextScreen);*/


                //finish();
            }
        });

        // Binding Click event to Button
       /* btnClose.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {

                Intent nextScreen = new Intent(getApplicationContext(), ScreenSecond.class);

                //Sending data to another Activity
                nextScreen.putExtra("name", inputName.getText().toString());

                Log.e("n", inputName.getText()+ "first screen");
                //Closing SecondScreen Activity
                Intent next = new Intent(getApplicationContext(), ScreenFirst.class);

                startActivity(next);
            }
        });
*/
    }
}