package com.example.task3_3;

import android.content.Context;
import android.content.Loader;
import android.os.Bundle;
import android.util.Log;

public class LoadAct extends Loader<Integer> {
    private final String LOG_TAG = "Log";
    private Integer num;

    public LoadAct(Context context,Bundle args) {
        super(context);
        num = args.getInt("value");
    }
    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        Log.d(LOG_TAG, hashCode() + " onStartLoading");
    }
    @Override
    protected void onStopLoading() {
        super.onStopLoading();
        Log.d(LOG_TAG, hashCode() + " onStopLoading");
    }

    @Override
    protected void onForceLoad() {
        super.onForceLoad();
        Log.d(LOG_TAG, hashCode() + " onForceLoad");
        Integer fact = 1;
        for (int i = 1; i<=num; i++){
            fact *= i;
        }
        getResultFromTask(fact);

    }
    void getResultFromTask(Integer result) {
        deliverResult(result);
    }
    @Override
    protected void onAbandon() {
        super.onAbandon();
        Log.d(LOG_TAG, hashCode() + " onAbandon");
    }

    @Override
    protected void onReset() {
        super.onReset();
        Log.d(LOG_TAG, hashCode() + " onReset");
    }
}
