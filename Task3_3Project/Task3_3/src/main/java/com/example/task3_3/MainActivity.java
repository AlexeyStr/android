package com.example.task3_3;

import android.app.LoaderManager;
import android.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity implements LoaderManager.LoaderCallbacks<Integer> {
    static final int LOADER_ID = 1;
    private TextView showFact;
    private  final String LOG_TAG = "myLogs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        showFact = (TextView) findViewById(R.id.viewFact);

    }

    public void onClickFact(View view){
        EditText editFact =(EditText)findViewById(R.id.editFact);
        int fact = Integer.parseInt(editFact.getText().toString());

        Bundle bFact = new Bundle();
        bFact.putInt("value", fact);
        getLoaderManager().initLoader(LOADER_ID, bFact, this);

        Loader<Integer> factLoader;
        factLoader = getLoaderManager().restartLoader(LOADER_ID, bFact,this);
        factLoader.forceLoad();
    }

    @Override
    public Loader<Integer> onCreateLoader(int i, Bundle bundle) {

        LoadAct fact = null;
        if (i == LOADER_ID){
            fact = new LoadAct(this,bundle);
        }
        return fact;
    }

    @Override
    public void onLoadFinished(Loader<Integer> stringLoader, Integer i) {
        showFact.setText("" + i);
    }


    @Override
    public void onLoaderReset(Loader<Integer> stringLoader) {
        Log.d(LOG_TAG, "onLoaderReset for loader " + stringLoader.hashCode());
    }
}
